const express = require("express");
const app = express();
const mongoose = require("mongoose");

mongoose.connect("mongodb://localhost/visitorManagement", {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
const db = mongoose.connection;
db.on("error", error => console.error(error));
db.once("open", () => console.log("connected to database"));

app.use(express.json());

const visitorsRouter = require("./routes/visitors");
app.use("/visitors", visitorsRouter);

const visiteesRouter = require("./routes/visitees");
app.use("/visitees", visiteesRouter);

const visitsRouter = require("./routes/visits");
app.use("/visits", visitsRouter);

const facesRouter = require("./routes/faces");
app.use("/faces", facesRouter);

app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "http://localhost:3000");
  next();
});

app.listen(3005, () => console.log("server started"));

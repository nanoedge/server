const express = require("express");
const router = express.Router();
const Visitor = require("../models/visitor");

module.exports = router;

// Get all subscribers
router.get("/", async (req, res) => {
  try {
    const visitor = await Visitor.find();
    res.json(visitor);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// Create one visitor
router.post("/", async (req, res) => {
  const visitor = new Visitor({
    name: req.body.name,
    surname: req.body.surname,
    email: req.body.email,
    company: req.body.company,
    phone: req.body.phone
  });

  try {
    const newVisitor = await visitor.save();
    res.status(201).json(newVisitor);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// Get one visitor
router.get("/single/:id", getVisitor, (req, res) => {
  res.json(res.visitor);
});

// Update one visitor
router.patch("/:id", getVisitor, async (req, res) => {
  if (req.body.name != null) {
    res.visitor.name = req.body.name;
  }

  if (req.body.surname != null) {
    res.visitor.surname = req.body.surname;
  }
  try {
    const updatedVisitor = await res.visitor.save();
    res.json(updatedVisitor);
  } catch {
    res.status(400).json({ message: err.message });
  }
});

// Delete one visitor
router.delete("/:id", getVisitor, async (req, res) => {
  try {
    await res.visitor.remove();
    res.json({ message: "Deleted This visitor" });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

async function getVisitor(req, res, next) {
  try {
    visitor = await Visitor.findById(req.params.id);
    if (visitor == null) {
      return res.status(404).json({ message: "Cant find visitor" });
    }
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }

  res.visitor = visitor;
  next();
}

module.exports = router;

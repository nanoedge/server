const express = require("express");
const router = express.Router();
const Visitee = require("../models/visitee");

module.exports = router;

// Get all visitees
router.get("/", async (req, res) => {
  try {
    const visitee = await Visitee.find();
    res.json(visitee);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// Create one visitee
router.post("/", async (req, res) => {
  const visitee = new Visitee({
    name: req.body.name,
    surname: req.body.surname,
    phone: req.body.phone,
    floor: req.body.floor,
    department: req.body.department,
    email: req.body.email
  });

  try {
    const newVisitee = await visitee.save();
    res.status(201).json(newVisitee);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// Get one visitee
router.get("/single/:id", getVisitee, (req, res) => {
  res.json(res.visitee);
});

// Update one visitee
router.patch("/:id", getVisitee, async (req, res) => {
  if (req.body.name != null) {
    res.visitee.name = req.body.name;
  }
  if (req.body.surname != null) {
    res.visitee.surname = req.body.surname;
  }
  if (req.body.phone != null) {
    res.visitee.phone = req.body.phone;
  }
  if (req.body.department != null) {
    res.visitee.department = req.body.department;
  }
  if (req.body.email != null) {
    res.visitee.email = req.body.email;
  }
  if (req.body.floor != null) {
    res.visitee.floor = req.body.floor;
  }
  try {
    const updatedVisitee = await res.visitee.save();
    res.json(updatedVisitee);
  } catch {
    res.status(400).json({ message: err.message });
  }
});

// Delete one visitee
router.delete("/:id", getVisitee, async (req, res) => {
  try {
    await res.visitee.remove();
    res.json({ message: "Deleted This visitee" });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

async function getVisitee(req, res, next) {
  try {
    visitee = await Visitee.findById(req.params.id);
    if (visitee == null) {
      return res.status(404).json({ message: "Cant find visitee" });
    }
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }

  res.visitee = visitee;
  next();
}

module.exports = router;

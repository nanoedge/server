const express = require("express");
const router = express.Router();
const Face = require("../models/face");

module.exports = router;

// Get all faces
router.get("/", async (req, res) => {
  try {
    const face = await Face.find();
    res.json(face);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// Create one face
router.post("/", async (req, res) => {
  const face = new Face({
    visitorId: req.body.visitorId,
    descriptors: req.body.descriptors
  });

  try {
    const newFace = await face.save();
    res.status(201).json(newFace);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// Get one visitor
router.get("/single/:id", getFace, (req, res) => {
  res.json(res.face);
});

// Update one visitor
router.patch("/:id", getFace, async (req, res) => {
  if (req.body.visitorId != null) {
    res.face.visitorId = req.body.visitorId;
  }

  if (req.body.descriptors != null) {
    res.face.descriptors = req.body.descriptors;
  }
  try {
    const updatedFace = await res.face.save();
    res.json(updatedFace);
  } catch {
    res.status(400).json({ message: err.message });
  }
});

// Delete one visitor
router.delete("/:id", getFace, async (req, res) => {
  try {
    await res.face.remove();
    res.json({ message: "Deleted This Face" });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

async function getFace(req, res, next) {
  try {
    face = await Face.findById(req.params.id);
    if (face == null) {
      return res.status(404).json({ message: "Cant find Face" });
    }
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }

  res.face = face;
  next();
}

module.exports = router;

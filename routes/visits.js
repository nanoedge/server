const express = require("express");
const router = express.Router();
const Visit = require("../models/visit");

module.exports = router;

// Get all subscribers
router.get("/", async (req, res) => {
  try {
    const visit = await Visit.find();
    res.json(visit);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// Create one visit
router.post("/", async (req, res) => {
  const visit = new Visit({
    time: req.body.time,
    visitorId: req.body.visitorId,
    visiteeId: req.body.visiteeId,
    purpose: req.body.purpose
  });

  try {
    const newVisit = await visit.save();
    res.status(201).json(newVisit);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// Get one visit

router.get("/single/:id", getVisit, (req, res) => {
  res.json(res.visit);
});

// Update one visit
router.patch("/:id", getVisit, async (req, res) => {
  if (req.body.name != null) {
    res.visit.name = req.body.name;
  }

  if (req.body.surname != null) {
    res.visit.surname = req.body.surname;
  }
  try {
    const updatedVisit = await res.visit.save();
    res.json(updatedVisit);
  } catch {
    res.status(400).json({ message: err.message });
  }
});

// Delete one visit
router.delete("/:id", getVisit, async (req, res) => {
  try {
    await res.visit.remove();
    res.json({ message: "Deleted This visit" });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

async function getVisit(req, res, next) {
  try {
    visit = await Visit.findById(req.params.id);
    if (visit == null) {
      return res.status(404).json({ message: "Cant find visit" });
    }
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }

  res.visit = visit;
  next();
}

module.exports = router;

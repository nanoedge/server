const mongoose = require("mongoose");

const visitSchema = new mongoose.Schema({
  time: {
    type: String,
    required: true
  },
  visitorId: {
    type: String,
    required: true
  },
  visiteeId: {
    type: String,
    required: true
  },
  purpose: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model("visit", visitSchema);

const mongoose = require("mongoose");

const visiteeSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  surname: {
    type: String,
    required: true
  },
  department: {
    type: String,
    required: true
  },
  floor: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model("visitee", visiteeSchema);

const mongoose = require("mongoose");

const faceSchema = new mongoose.Schema({
  visitorId: {
    type: String,
    required: true
  },
  descriptors: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model("face", faceSchema);
